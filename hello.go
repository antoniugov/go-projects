package main

import (
	"fmt"

	"bitbucket.org/antoniugov/stringutil"
)

func main() {
	fmt.Println("Using stringutil...")
	fmt.Println(stringutil.Reverse("!oG ,olleH"))
}
